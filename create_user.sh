#!/bin/bash

DIRECTORY=`pwd`
cd $DIRECTORY

echo $P1 > $DIRECTORY/list.txt

sed 's/\s\+/\n/g' $DIRECTORY/list.txt > sorted_list.txt

while read LINE
    do
sed  "s/&base/$LINE/g" user_base > $DIRECTORY/uc_$LINE.sql
sed  "s/&uname/$LINE/g" grant_base > $DIRECTORY/gr_$LINE.sql
done < $DIRECTORY/sorted_list.txt  


cat $DIRECTORY/sorted_list.txt

sleep 2
sed -i "s/&role/$P2/g" gr*sql

rm consolidated.sql
cat uc_*sql >> all_uc.sql
cat gr*sql >> all_gr.sql
cat all_uc.sql >> consolidated.sql
cat all_gr.sql >> consolidated.sql

rm all* gr* uc*

cat consolidated.sql
